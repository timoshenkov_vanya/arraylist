package arrayList;

import java.util.Arrays;

public class MyList<T> {
    private int realSize;
    private Object[] basedList;

    MyList() {
        int standardSize = 10;
        this.basedList = new Object[standardSize];
        this.realSize = 0;
    }

    MyList(int inputSize) {
        this.basedList = new Object[inputSize];
        this.realSize = 0;
    }

    public boolean isEmpty() {
        return this.basedList[0] == null;
    }

    public void remove(int indexOfThing) {
        if (indexOfThing > this.basedList.length - 1) {throw new RuntimeException("Такого индекса нет в списке");}
        realSize -= 1;
        Object[] result = new Object[basedList.length - 1];
        System.arraycopy(this.basedList, 0, result, 0, indexOfThing);
        System.arraycopy(this.basedList, indexOfThing + 1, result, indexOfThing,
                basedList.length - indexOfThing - 1);
        this.basedList = result;
    }

    public void remove(Object thing) {
        boolean isElementFinded = false;
        int indexOfThing = 0;
        Object[] result = new Object[basedList.length - 1];
        for (int i = 0; i < this.basedList.length; i++) {
            if (this.basedList[i] == thing) {
                this.basedList[i] = null;
                indexOfThing = i;
                isElementFinded = true;
                break;
            }
        }
        if (!isElementFinded) throw new RuntimeException("Такого элемента нет в списке");
        realSize -= 1;
        System.arraycopy(this.basedList, 0, result, 0, indexOfThing);
        System.arraycopy(this.basedList, indexOfThing + 1, result, indexOfThing,
                basedList.length - indexOfThing - 1);
        this.basedList = result;
    }

    public void clear() {
        Arrays.fill(this.basedList, null);
    }

    public Object get(int index) {
        return this.basedList[index];
    }
    private Object[] copyBiggerList(T newElement) {
        Object[] newArr = new Object[this.realSize];
        System.arraycopy(this.basedList, 0, newArr, 0, basedList.length);
        newArr[this.realSize - 1] = newElement;
        return newArr;
    }

    public void add(T newElement) {
        this.realSize += 1;
        if (this.isEmpty()) {
            basedList[0] = newElement;
        }
        if (this.realSize > this.basedList.length) {
            this.basedList = this.copyBiggerList(newElement);
        } else this.basedList[realSize - 1] = newElement;
    }

    public void add(int index, T element) {
        if (this.isEmpty()) throw new RuntimeException("Список пуст");
        Object[] result = new Object[basedList.length + 1];
        System.arraycopy(this.basedList, 0, result, 0, index);
        result[index] = element;
        System.arraycopy(this.basedList, index, result, index + 1,
                basedList.length - index);
        this.basedList = result;
    }

    public int size() {
        return this.basedList.length;
    }

    public int getRealSize() {
        return this.realSize;
    }


    public boolean contains(T thing) {
        for (Object o : this.basedList) {
            if (o == thing) return true;
        }
        return false;
    }

    public void PythonModeOut() {
        StringBuilder line = new StringBuilder("[");
        for (Object x : this.basedList) line.append(x).append(" ");
        System.out.println(line.toString().trim() + "]");
    }
}
